package oop.lab1;
import javax.print.attribute.standard.MediaSize.Other;


//TODO Write class Javadoc
/**
 * 
 * @author kitipoom
 *
 */
public class Student extends Person {
	private long id;
	
	//TODO Write constructor Javadoc
	public Student(String name, long id) {
		super(name); // name is managed by Person
		this.id = id;
	}

	/** return a string representation of this Student. */
	public String toString() {
		return String.format("Student %s (%d)", getName(), id);
	}
	public long getId(Student other){
		return other.id;
		
		
	}

	//TODO Write equals
	public boolean equals(Student other) {
		if(this.getClass().equals(other.getClass())){
			return this.id == other.id;
		}
		else{
			return false;
		}
		
		//return this.id == other.id;
	}
	public boolean equals(Object other) {
		if(this.getClass().equals(other.getClass())){
			return this.id == getId((Student) other);
		}
		else{
			return false;
		}
		
		//return this.id == other.id;
	}
}
